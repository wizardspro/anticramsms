-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 20, 2014 at 12:42 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `_sms`
--
CREATE DATABASE IF NOT EXISTS `_sms` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `_sms`;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('8925cf1243e294872d4f14ebf41ff712', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 1397997082, 'a:10:{s:9:"user_data";s:0:"";s:7:"user_id";s:1:"4";s:8:"username";s:8:"username";s:8:"password";s:32:"e10adc3949ba59abbe56e057f20f883e";s:9:"last_name";s:4:"pogi";s:11:"middle_name";s:6:"tdfgvb";s:10:"first_name";s:3:"Pio";s:9:"contactNo";s:14:"(656) 845-3287";s:5:"email";s:14:"asdfasdf@y.com";s:12:"is_logged_in";b:1;}');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `notifid` int(11) NOT NULL,
  `smslog` datetime NOT NULL,
  PRIMARY KEY (`notifid`),
  UNIQUE KEY `notifid` (`notifid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE IF NOT EXISTS `quiz` (
  `quiz_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(25) NOT NULL,
  `quiz_name` varchar(25) NOT NULL,
  `quiz_category` text NOT NULL,
  `sched` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `token` varchar(28) NOT NULL,
  `msg` varchar(160) NOT NULL,
  PRIMARY KEY (`quiz_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`quiz_id`, `subject_id`, `quiz_name`, `quiz_category`, `sched`, `token`, `msg`) VALUES
(45, 20, 'asdfasdf', 'Exam', '0000-00-00 00:00:00', '', ''),
(46, 20, 'sdfaf', 'Assignment', '0000-00-00 00:00:00', '', ''),
(47, 20, 'jampot', 'Project', '0000-00-00 00:00:00', '', ''),
(48, 20, 'timothy', 'Exam', '2014-04-21 23:22:00', '', ''),
(49, 20, 'tong', 'Exam', '0000-00-00 00:00:00', '', ''),
(50, 20, 'asdf', 'Exam', '0000-00-00 00:00:00', '', ''),
(51, 20, 'BUBU', 'Exam', '2014-04-03 11:15:00', '', ''),
(52, 20, 'asdfasdf', 'Duty', '2014-04-03 11:15:00', '0', '0'),
(53, 20, 'Juju', 'Exam', '2014-04-03 11:15:00', '0', '0'),
(54, 20, 'adfadsf', 'Exam', '2014-04-03 11:15:00', '0', 'asdfadsfaf'),
(55, 20, 'token', 'Exam', '2014-04-03 11:15:00', 'jdj1Wtt30136CKTrANL8Dnhz5ov1', 'asdfasdfadsfa'),
(56, 20, 'token 2`', 'Exam', '2014-04-03 11:15:00', 'jdj1Wtt30136CKTrANL8Dnhz5ov1', 'asdfas');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `userid` int(11) NOT NULL,
  `subjectid` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `grade` double NOT NULL,
  `datetimeofexam` datetime NOT NULL,
  `notificationdatetime` datetime NOT NULL,
  `profname` varchar(45) NOT NULL,
  `room` varchar(45) NOT NULL,
  `day` varchar(25) NOT NULL,
  `time` time NOT NULL,
  `endclass` time NOT NULL,
  PRIMARY KEY (`subjectid`),
  UNIQUE KEY `subjectid` (`subjectid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`userid`, `subjectid`, `subject`, `category`, `grade`, `datetimeofexam`, `notificationdatetime`, `profname`, `room`, `day`, `time`, `endclass`) VALUES
(0, 8, '0', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '0', '0', '00:00:00', '00:00:00'),
(0, 9, '0', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '0', '0', '00:00:00', '00:00:00'),
(0, 10, '0', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '0', '0', '00:00:00', '00:00:00'),
(0, 11, '0', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '0', '0', '00:00:00', '00:00:00'),
(0, 12, '0', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '0', '0', '00:00:00', '00:00:00'),
(0, 13, '0', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '0', '0', '00:00:00', '00:00:00'),
(8, 19, 'Math', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'sdasdasdasdasdasdasd', '123132412312', 'Monday', '14:22:00', '00:00:00'),
(4, 20, 'Pogi Problems', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Pio', '215', 'Tuesday', '23:00:00', '01:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `middle_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `contact_no` varchar(45) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`userid`, `first_name`, `middle_name`, `last_name`, `email`, `contact_no`, `gender`, `username`, `password`, `date_created`, `date_update`) VALUES
(4, 'Pio', 'tdfgvb', 'pogi', 'asdfasdf@y.com', '(656) 845-3287', 'Male', 'username', 'e10adc3949ba59abbe56e057f20f883e', '2014-02-10 02:27:19', '2014-03-18 12:44:37'),
(5, 'asdfj', '', 'lasdjf', 'asdfasdf@y.com', '(999) 999-9999', 'Male', 'awdeeee_askdfj1', 'e10adc3949ba59abbe56e057f20f883e', '2014-02-10 02:27:45', '0000-00-00 00:00:00'),
(6, 'Pio', 'qwerty', 'Lozanoo', 'niel_pio@hotmail.com', '(999) 999-9999', 'Male', 'username1', 'e10adc3949ba59abbe56e057f20f883e', '2014-02-10 10:26:14', '0000-00-00 00:00:00'),
(7, 'TestOne', '', 'TestTwooo', 'test@test.com', '(918) 583-6853', 'Male', 'test123', '5f4dcc3b5aa765d61d8327deb882cf99', '2014-02-11 18:27:21', '2014-02-11 18:28:52'),
(8, 'Kenneth', 'Nieto', 'Flores', 'newken21@gmail.com', '(639) 494-5645', 'Male', 'newken', 'a2da99c1e15d32619324b3b9b50ead61', '2014-03-10 11:41:14', '2014-03-10 16:07:11');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
